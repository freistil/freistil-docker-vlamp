FROM freistil/lamp:latest
MAINTAINER Jochen Lillich <jochen@freistil.it>

# Install packages
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
  apt-get install -y \
    varnish \
  && apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# Varnish
ADD varnish/start.sh /setup/varnish-start.sh
ADD varnish/supervisord.conf /etc/supervisor/conf.d/supervisord-varnish.conf
ENV VARNISH_LISTEN_PORT=80
ENV VARNISH_ADMIN_PORT=6082
ENV VARNISH_VCL_FILE="default.vcl"
ENV VARNISH_SECRET_FILE="secret"
ENV VARNISH_MEMORY="256m"

# Apache
RUN sed -i -e 's/Listen 80/Listen 8080/g' /etc/apache2/ports.conf
RUN sed -i -e 's/VirtualHost \*:80/VirtualHost *:8080/' /etc/apache2/sites-available/000-default.conf

# Boot container
RUN chmod 755 /run.sh /setup/*.sh
CMD ["/run.sh"]
