#!/bin/bash
exec varnishd -F \
  -a :$VARNISH_LISTEN_PORT \
  -T localhost:$VARNISH_ADMIN_PORT \
  -f /etc/varnish/$VARNISH_VCL_FILE \
  -S /etc/varnish/$VARNISH_SECRET_FILE \
  -s malloc,$VARNISH_MEMORY
